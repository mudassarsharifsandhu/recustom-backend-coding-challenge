import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import * as fs from 'fs';

import { AppModule } from '../src/app.module';
import { INestApplication } from '@nestjs/common';

describe('PdfController (e2e)', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  const clipData = {
    title: 'PDF Title',
    content: 'Test PDF Content',
    clip_media: [
      { source: 'love.png', type: 'IMAGE' },
      { source: 'march.png', type: 'IMAGE' },
    ],
  };

  it('(POST) - request for new pdf', async () => {
    const response = await request(app.getHttpServer())
      .post('/pdf/generate')
      .send(clipData)
      .expect(201);

    expect(response.body).toHaveProperty('url');
    expect(fs.existsSync(response.body.url)).toBe(true);

    fs.unlinkSync(response.body.url);
  }, 10000);

  afterAll(async () => {
    await app.close();
  });
});
